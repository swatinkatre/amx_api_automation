*** Settings ***
Library           Selenium2Library
Library           RequestsLibrary
Library           SudsLibrary
Library           OperatingSystem
Library           openpyxl
Library           String
Library           ExcelLibrary
Library           XML
Resource          API.txt
Library           ../CustomLibrary/Test.py
Library           ../CustomLibrary/UpdateExcelSheet.py

*** Keywords ***
GetCount
    [Arguments]    ${Datasheetname}
    @{Bulk_Values}    Readvalue All    1    ${API_Excel_Path}/API_excel.xlsx    ${Datasheetname}
    ${INDEX}    Set Variable    1
    ${count}    Set Variable    @{Bulk_Values}[1]
    ${count}    Convert To Integer    ${count}
    ${count}=    Evaluate    ${count}+1
    [Return]    ${count}
