*** Settings ***
Library           Selenium2Library
Library           RequestsLibrary
Library           SudsLibrary
Library           OperatingSystem
Library           openpyxl
Library           String
Library           ExcelLibrary
Library           XML
Resource          API.txt
Library           ../CustomLibrary/Test.py
Library           ../CustomLibrary/UpdateExcelSheet.py

*** Test Cases ***
Easy Directory Number Search
    @{Bulk_Values}    Readvalue All    1    ${API_Excel_Path}/API_excel.xlsx    DirectoryApi
    ${INDEX}    Set Variable    1
    ${count}    Set Variable    @{Bulk_Values}[1]
    ${count}    Convert To Integer    ${count}
    ${count}=    Evaluate    ${count}+1
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    Create Session    Session    ${API_URL}
    \    @{Bulk_Values}    Readvalue All    ${INDEX}    ${API_Excel_Path}/API_excel.xlsx    DirectoryApi
    \    ${plcodePub}    Set Variable    @{Bulk_Values}[2]
    \    ${dirnum}    Set Variable    @{Bulk_Values}[3]
    \    ${submIdPub}    Set Variable    @{Bulk_Values}[4]
    \    ${npcodePub}    Set Variable    @{Bulk_Values}[5]
    \    ${Request}    Get File    ${API_Resource_Path}/Easy Directory Number Search Request.txt
    \    ${Convert}    Create Dictionary    content-type=text/xml
    \    ${Request}    Replace String    ${Request}    PLLCode    ${plcodePub}
    \    ${dirnum}    Convert To Integer    ${dirnum}
    \    ${submIdPub}    Convert To String    ${submIdPub}
    \    ${npcodePub}    Replace String    ${Request}    SimNumResource    ${npcodePub}
    \    ${submIdPub}    Replace String    ${Request}    SubmitPlcode    ${submIdPub}
    \    ${INDEX}=    Evaluate    ${INDEX}+1
    \    Log    ${Request}
    \    #verifying the resource pending
    \    #${Response}    Post Request    Session    /wsi/services    data=${test}    headers=${Convert}
    \    # Should Contain    ${Response}    smId
    \    #Should Contain    ${Response}    smId
