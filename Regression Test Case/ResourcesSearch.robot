*** Settings ***
Library           Selenium2Library
Library           RequestsLibrary
Library           SudsLibrary
Library           OperatingSystem
Library           openpyxl
Library           String
Library           ExcelLibrary
Library           XML
Resource          API.txt
Library           ../CustomLibrary/Test.py
Library           ../CustomLibrary/UpdateExcelSheet.py

*** Test Cases ***
ResourceSearchAPI
    @{Bulk_Values}    Readvalue All    1    ${API_Excel_Path}/API_excel.xlsx    ResourceSearchAPI
    ${INDEX}    Set Variable    1
    ${count}    Set Variable    @{Bulk_Values}[1]
    ${count}    Convert To Integer    ${count}
    ${count}=    Evaluate    ${count}+1
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    Create Session    Session    ${API_URL}
    \    @{Bulk_Values}    Readvalue All    ${INDEX}    ${API_Excel_Path}/API_excel.xlsx    ResourceSearchAPI
    \    ${plcode}    Set Variable    @{Bulk_Values}[2]
    \    ${simnum}    Set Variable    @{Bulk_Values}[3]
    \    ${submiPub}    Set Variable    @{Bulk_Values}[4]
    \    ${Request}    Get File    ${API_Resource_Path}/ResourceSearch.txt
    \    ${Convert}    Create Dictionary    content-type=text/xml
    \    ${Request}    Replace String    ${Request}    PLLCode    ${plcode}
    \    ${simnum}    Convert To Integer    ${simnum}
    \    ${simnum}    Convert To String    ${simnum}
    \    ${Request}    Replace String    ${Request}    SimNumResource    ${simnum}
    \    ${Request}    Replace String    ${Request}    SubmitPlcode    ${submiPub}
    \    ${INDEX}=    Evaluate    ${INDEX}+1
    \    Log    ${Request}
    \    #verifying the resource pending
    \    #${Response}    Post Request    Session    /wsi/services    data=${test}    headers=${Convert}
    \    # Should Contain    ${Response}    smId
    \    #Should Contain    ${Response}    smId
